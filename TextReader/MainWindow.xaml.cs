﻿using System;
using System.Windows;
using System.Windows.Input;
using TextReader.ContentClasses;
using TextReader.Exceptions;

namespace TextReader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IContent currentContent;

        public MainWindow()
        {
            InitializeComponent();
            
            MainTextBox.MatchCountChanged += (count) => SearchPanel.MatchCount = count;
            SearchPanel.TextToFindChanged += MainTextBox.OnSearch;
            SearchPanel.PreviousButton.Click += OnPrevSearchResult;
            SearchPanel.NextButton.Click += OnNextSearchResult;
        }

        public void OnNextSearchResult(object sender, EventArgs e)
            => MainTextBox.NextSearchResult();

        public void OnPrevSearchResult(object sender, EventArgs e)
            => MainTextBox.PrevSearchResult();

        private void ExecutedFindCommand(object sender, ExecutedRoutedEventArgs e)
            => SearchPanel.Show();

        private void OnSearchButton_Click(object sender, RoutedEventArgs e)
            => SearchPanel.SwitchVisibility();

        private void OnOpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            IContentProvider provider = new TextContentProvider();
            CreateAndSetContent(provider);
        }
        
        private void OnSaveFileButton_Click(object sender, RoutedEventArgs e)
        {
            // In case during saving user switch to another content
            var contentToSave = currentContent;
            if (contentToSave != null)
            {
                contentToSave.SaveAsync();
            }           
        }

        private void OnGetFromUrlButton_Click(object sender, RoutedEventArgs e)
        {
            IContentProvider provider = new WebContentProvider(UrlField.Text);
            CreateAndSetContent(provider);
        }

        private void Provider_OnLoadEnd(object sender, EventArgs e)
        {
            LoadingIcon.Visibility = Visibility.Hidden;
        }

        private void Provider_OnLoadBegin(object sender, EventArgs e)
        {
            LoadingIcon.Visibility = Visibility.Visible;
        }

        private void CreateAndSetContent(IContentProvider provider)
        {
            provider.OnLoadBegin += Provider_OnLoadBegin;
            provider.OnLoadEnd += Provider_OnLoadEnd;
            IContentSaver saver = new SaverToFile();
            IContent content = new Content(provider, saver);
            SetNewContent(content);
        }

        private async void SetNewContent(IContent content)
        {
            try
            {
                currentContent = content;
                MainTextBox.ItemsSource = await content.GetAsync();
            }
            catch(ReadContentException e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
