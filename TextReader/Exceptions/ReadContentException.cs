﻿using System;

namespace TextReader.Exceptions
{
    public class ReadContentException : Exception
    {
        public ReadContentException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ReadContentException(string message)
            : base(message)
        {
        }

        public ReadContentException()
        {
        }
    }
}
