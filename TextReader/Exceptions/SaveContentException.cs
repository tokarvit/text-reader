﻿using System;

namespace TextReader.Exceptions
{
    public class SaveContentException : Exception
    {
        public SaveContentException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public SaveContentException(string message)
            : base(message)
        {
        }

        public SaveContentException()
        {
        }
    }
}
