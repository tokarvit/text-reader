﻿using System;
using System.Windows.Controls;
using System.Windows.Documents;

namespace TextReader.Views
{
    public partial class TextBlockWithHighlight : TextBlock
    {
        public void ResetText()
        {
            Inlines.Clear();
            Inlines.Add(Text);
        }

        public int UpdateText()
        {
            if (string.IsNullOrEmpty(HighlightPhrase)
                 || string.IsNullOrEmpty(Text))
            {
                ResetText();
                return 0;
            }

            var prevText = Text;
            Inlines.Clear();

            int matchCount = 0;
            while (true) {
                int index = prevText.IndexOf(HighlightPhrase, StringComparison.OrdinalIgnoreCase);
                                
                if (index < 0)
                {
                    Inlines.Add(prevText);
                    break;
                }
                else
                {
                    matchCount++;

                    if (index > 0)
                    {
                        Inlines.Add(prevText.Substring(0, index));
                    }

                    Inlines.Add(new Run(prevText.Substring(index, HighlightPhrase.Length))
                    {
                        Background = HighlightColor
                    });
                    
                    prevText = prevText.Substring(index + HighlightPhrase.Length);
                }
            }
            

            return matchCount;
        }
    }
}
