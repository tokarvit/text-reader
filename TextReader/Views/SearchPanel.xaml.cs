﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TextReader.Views
{
    /// <summary>
    /// Interaction logic for SearchPanel.xaml
    /// </summary>
    public partial class SearchPanel : UserControl
    {
        public delegate void TextToFindChangedHandler(string textToFind, bool needFilter);
        public event TextToFindChangedHandler TextToFindChanged;

        public int MatchCount {
            get { return (int)MatchCountLabel.Content; }
            set { MatchCountLabel.Content = value; }
        }

        public SearchPanel()
        {
            InitializeComponent();
        }

        private void FindButton_Click(object sender, RoutedEventArgs e)
        {
            TextToFindChanged?.Invoke(TextToFind.Text, false);
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            TextToFindChanged?.Invoke(TextToFind.Text, true);
        }

        public void Show()
        {
            Visibility = Visibility.Visible;
            Keyboard.Focus(TextToFind);
            TextToFind.Focus();
        }

        public void Hide()
        {
            Visibility = Visibility.Hidden;
            MatchCount = 0;
            TextToFindChanged?.Invoke(string.Empty, false);
        }

        public void SwitchVisibility()
        {
            if (Visibility == Visibility.Visible)
                Hide();
            else
                Show();
        }

    }
}
