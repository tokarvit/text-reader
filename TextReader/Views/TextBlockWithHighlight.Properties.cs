﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TextReader.Views
{
    public partial class TextBlockWithHighlight : TextBlock
    {
        public new string Text {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public string HighlightPhrase {
            get { return (string)GetValue(HighlightPhraseProperty); }
            set { SetValue(HighlightPhraseProperty, value); }
        }

        public Brush HighlightColor {
            get { return (Brush)GetValue(HighlightColorProperty); }
            set { SetValue(HighlightColorProperty, value); }
        }

        public new static readonly DependencyProperty TextProperty =
            DependencyProperty.Register(
                "Text", 
                typeof(string),
                typeof(TextBlockWithHighlight), 
                new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.AffectsRender,
                    new PropertyChangedCallback(UpdateText))
                );

        public static readonly DependencyProperty HighlightPhraseProperty =
            DependencyProperty.Register(
                "HighlightPhrase",
                typeof(string),
                typeof(TextBlockWithHighlight),
                new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.AffectsRender,
                    new PropertyChangedCallback(UpdateText))
                );

        public static readonly DependencyProperty HighlightColorProperty =
            DependencyProperty.Register(
                "HighlightColor",
                typeof(Brush),
                typeof(TextBlockWithHighlight),
                new FrameworkPropertyMetadata(Brushes.Yellow, FrameworkPropertyMetadataOptions.AffectsRender,
                    new PropertyChangedCallback(UpdateText))
                );

        private static void UpdateText(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as TextBlockWithHighlight).UpdateText();
        }
    }
}
