﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TextReader.Views
{
    public partial class ContentView : ListBox
    {
        public delegate void MatchCountChangedHandler(int count);
        public event MatchCountChangedHandler MatchCountChanged;

        public string HighlightPhrase {
            get { return (string)GetValue(HighlightPhraseProperty); }
            set { SetValue(HighlightPhraseProperty, value); }
        }

        public Brush HighlightColor {
            get { return (Brush)GetValue(HighlightBrushProperty); }
            set { SetValue(HighlightBrushProperty, value); }
        }

        public static readonly DependencyProperty HighlightPhraseProperty =
            DependencyProperty.Register(
                "HighlightPhrase", 
                typeof(string),
                typeof(ContentView), 
                new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.AffectsRender,
                    new PropertyChangedCallback(UpdateView))
                );

        public static readonly DependencyProperty HighlightBrushProperty =
            DependencyProperty.Register(
                "HighlightBrush", 
                typeof(Brush),
                typeof(ContentView), 
                new FrameworkPropertyMetadata(Brushes.Yellow, FrameworkPropertyMetadataOptions.AffectsRender,
                    new PropertyChangedCallback(UpdateView))
                );
        
        private static void UpdateView(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as ContentView).SearchTextInItems();
        }
    }
}
