﻿using System;
using System.Collections;
using System.Windows.Controls;
using System.Windows.Input;
using TextReader.ContentClasses;

namespace TextReader.Views
{
    public partial class ContentView : ListBox
    {
        private const double VERTICAL_SCROLL_SPEED = 1;
        private const double HORIZONTAL_SCROLL_SPEED = 10;
        
        private ScrollViewer scrollViewer;
        private ArrayList foundItems = new ArrayList();
        private int currentFoundItem = -1;

        public override void EndInit()
        {
            base.EndInit();

            Loaded += (s, e) =>
            {
                scrollViewer = Utils.FindFirstChildByType<ScrollViewer>(this);
            };

            SelectionChanged += (s, e) =>
            {
                ScrollIntoView(SelectedItem);

                ((ListBoxItem)ItemContainerGenerator
                  .ContainerFromItem(SelectedItem))?.Focus();

                scrollViewer.PageLeft();
            };
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.PageUp:
                    scrollViewer.PageUp();
                    break;
                case Key.PageDown:
                    scrollViewer.PageDown();
                    break;
                case Key.End:
                    scrollViewer.ScrollToEnd();
                    break;
                case Key.Home:
                    scrollViewer.ScrollToHome();
                    break;
                case Key.Up:
                    scrollViewer.ScrollToVerticalOffset(
                        scrollViewer.VerticalOffset - VERTICAL_SCROLL_SPEED < 0
                        ? 0 : scrollViewer.VerticalOffset - VERTICAL_SCROLL_SPEED);
                    break;
                case Key.Down:
                    scrollViewer.ScrollToVerticalOffset(
                        scrollViewer.VerticalOffset + VERTICAL_SCROLL_SPEED > scrollViewer.Height
                        ? scrollViewer.Height : scrollViewer.VerticalOffset + VERTICAL_SCROLL_SPEED);
                    break;
                case Key.Left:
                    scrollViewer.ScrollToHorizontalOffset(
                        scrollViewer.HorizontalOffset + HORIZONTAL_SCROLL_SPEED < 0
                        ? 0 : scrollViewer.HorizontalOffset - HORIZONTAL_SCROLL_SPEED);
                    break;
                case Key.Right:
                    scrollViewer.ScrollToHorizontalOffset(
                        scrollViewer.HorizontalOffset + HORIZONTAL_SCROLL_SPEED > scrollViewer.Width
                        ? scrollViewer.Width : scrollViewer.HorizontalOffset + HORIZONTAL_SCROLL_SPEED);
                    break;
                default: return;
            }

            e.Handled = true;
        }

        protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            base.OnItemsSourceChanged(oldValue, newValue);

            Keyboard.Focus(this);
            SelectedIndex = -1;
            
            if (!string.IsNullOrEmpty(HighlightPhrase))
            {
                SearchTextInItems();
            }
            else
            {
                currentFoundItem = -1;
                foundItems.Clear();
            }
        }

        private bool DoesLineMatch(ContentLine line, string textToFind)
            => line.Line.IndexOf(textToFind, StringComparison.OrdinalIgnoreCase) >= 0;

        public void OnSearch(string textToFind, bool needFilter)
        {
            if (needFilter)
            {
                Items.Filter = (l) => DoesLineMatch(l as ContentLine, textToFind);
            }
            else
            {
                Items.Filter = null;
            }

            HighlightPhrase = textToFind;
        }

        private void SearchTextInItems()
        {
            currentFoundItem = -1;
            foundItems.Clear();

            for (int i = 0; i < Items.Count; i++)
            {
                if (DoesLineMatch(Items[i] as ContentLine, HighlightPhrase))
                {
                    foundItems.Add(i);

                    // Set the nearest matched item's index
                    if(currentFoundItem < 0
                        && i >= scrollViewer.VerticalOffset)
                    {
                        currentFoundItem = foundItems.Count - 1;
                    }
                }
            }

            MatchCountChanged?.Invoke(foundItems.Count);
            NextSearchResult();
        }

        public void NextSearchResult()
        {
            if (foundItems.Count > 0)
            {
                currentFoundItem = (currentFoundItem + 1) % foundItems.Count;
                SelectedIndex = (int)foundItems[currentFoundItem];
            }
        }

        public void PrevSearchResult()
        {
            if (foundItems.Count > 0)
            {
                currentFoundItem--;
                currentFoundItem = (currentFoundItem < 0)
                    ? foundItems.Count - 1
                    : currentFoundItem;
                SelectedIndex = (int)foundItems[currentFoundItem];
            }
        }

    }
}
