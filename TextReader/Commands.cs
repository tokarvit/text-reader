﻿using System.Windows.Input;

namespace TextReader
{
    public static class Commands
    {
        public static RoutedCommand FindCommand = new RoutedCommand();
        public static RoutedCommand NextSearchResultCommand = new RoutedCommand();
        public static RoutedCommand PrevSearchResultCommand = new RoutedCommand();
    }
}
