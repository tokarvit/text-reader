﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TextReader.ContentClasses;

namespace TextReader
{
    public interface IContentProvider
    {
        event EventHandler OnLoadBegin;
        event EventHandler OnLoadEnd;

        Task<IEnumerable<ContentLine>> GetAsync();
    }
}
