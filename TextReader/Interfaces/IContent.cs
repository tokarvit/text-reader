﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TextReader.ContentClasses;

namespace TextReader
{
    public interface IContent
    {
        bool IsLoading { get; }
        Task SaveAsync();
        Task<IEnumerable<ContentLine>> GetAsync();
    }
}
