﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TextReader.ContentClasses;

namespace TextReader
{
    public interface IContentSaver
    {
        Task SaveAsync(IEnumerable<ContentLine> lines);
    }
}
