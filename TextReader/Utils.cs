﻿using System.Windows;
using System.Windows.Media;

namespace TextReader
{
    public static class Utils
    {
        public static T FindFirstChildByType<T>(DependencyObject obj) where T: class
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                var elem = VisualTreeHelper.GetChild(obj, i) as DependencyObject;
                T searchElem = elem as T;
                if (searchElem != null)
                {
                    return searchElem;
                }

                searchElem = FindFirstChildByType<T>(elem);
                if (searchElem != null)
                {
                    return searchElem;
                }
            }

            return null;
        }
    }
}
