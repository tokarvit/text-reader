﻿namespace TextReader.ContentClasses
{
    public class ContentLine
    {
        public string Line { get; set; }

        public ContentLine(string line)
        {
            Line = line;
        }
    }
}
