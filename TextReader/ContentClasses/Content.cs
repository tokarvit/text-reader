﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TextReader.ContentClasses
{
    public class Content : IContent
    {
        public bool IsLoading { get; private set; }

        private IEnumerable<ContentLine> lines;
        private IContentProvider _provider;
        private IContentSaver _saver;
        
        public Content(IContentProvider provider, IContentSaver saver)
        {
            _provider = provider;
            _saver = saver;
        }

        public async Task<IEnumerable<ContentLine>> GetAsync()
        {
            if (lines == null)
            {
                IsLoading = true;
                lines = await _provider.GetAsync();
                IsLoading = false;
            }

            return lines;
        }

        public async Task SaveAsync() => await _saver.SaveAsync(lines);
    }
}
