﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TextReader.Exceptions;

namespace TextReader.ContentClasses
{
    public class SaverToFile : IContentSaver
    {
        private const int DefaultBufferSize = 4096;
        private const FileOptions DefaultOptions
            = FileOptions.Asynchronous | FileOptions.SequentialScan;

        public async Task SaveAsync(IEnumerable<ContentLine> lines)
        {
            var fileName = SelectTextFileName();
            await WriteToFileAsync(lines, fileName);
        }

        private string SelectTextFileName()
        {
            var dialog = new SaveFileDialog()
            {
                Filter = "Text Files|*.txt",
                Title = "Save to file"
            };

            if (dialog.ShowDialog() == true)
            {
                return dialog.FileName;
            }
            else
            {
                throw new SaveContentException("Please choose appropriate file name");
            }
        }

        private async Task WriteToFileAsync(IEnumerable<ContentLine> lines, string fileName)
        {
            try
            {
                using (var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write,
                    FileShare.Read, DefaultBufferSize, DefaultOptions))
                using (var writer = new StreamWriter(stream))
                {
                    foreach (var l in lines)
                    {
                        await writer.WriteLineAsync(l.Line);
                    }
                }
            }
            catch (Exception e)
            {
                throw new SaveContentException("Failed to save data to file " + fileName, e);
            }
        }
    }
}
