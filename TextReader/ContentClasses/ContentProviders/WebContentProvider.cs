﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using TextReader.ContentClasses;
using TextReader.Exceptions;

namespace TextReader
{
    public class WebContentProvider : IContentProvider
    {
        public event EventHandler OnLoadBegin;
        public event EventHandler OnLoadEnd;

        private readonly string url;

        public WebContentProvider(string url)
        {
            this.url = url;
        }

        public async Task<IEnumerable<ContentLine>> GetAsync()
        {
            OnLoadBegin?.Invoke(this, new EventArgs());

            var lines = new List<ContentLine>();

            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead(url))
                using (var reader = new StreamReader(stream))
                {
                    string line;
                    while ((line = await reader.ReadLineAsync()) != null)
                    {
                        lines.Add(new ContentLine(line));
                    }
                }
            }
            catch(Exception e)
            {
                throw new ReadContentException("Failed to load data from url " + url, e);
            }
            finally
            {
                OnLoadEnd?.Invoke(this, new EventArgs());
            }

            return lines;
        }
        
    }
}
