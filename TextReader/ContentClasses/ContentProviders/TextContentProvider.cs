﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TextReader.ContentClasses;
using TextReader.Exceptions;

namespace TextReader
{
    public class TextContentProvider : IContentProvider
    {
        public event EventHandler OnLoadBegin;
        public event EventHandler OnLoadEnd;

        private const int DefaultBufferSize = 4096;
        private const FileOptions DefaultOptions 
            = FileOptions.Asynchronous | FileOptions.SequentialScan;
        
        public async Task<IEnumerable<ContentLine>> GetAsync()
        {
            var fileName = SelectTextFile();
            return await ReadFromFileAsync(fileName);
        }

        public async Task<IEnumerable<ContentLine>> ReadFromFileAsync(string fileName)
        {
            OnLoadBegin?.Invoke(this, new EventArgs());

            var lines = new List<ContentLine>();

            try { 
                using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, 
                    FileShare.Read, DefaultBufferSize, DefaultOptions))
                using (var reader = new StreamReader(stream))
                {
                    string line;
                    while ((line = await reader.ReadLineAsync()) != null)
                    {
                        lines.Add(new ContentLine(line));
                    }
                }
            }
            catch(Exception e)
            {
                throw new ReadContentException("Failed to load data located " + fileName, e);
            }
            finally
            {
                OnLoadEnd?.Invoke(this, new EventArgs());
            }

            return lines;
        }

        private string SelectTextFile()
        {
            var dialog = new OpenFileDialog
            {
                Filter = "Text Files|*.txt",
                Title = "Select a Text File"
            };

            if (dialog.ShowDialog() == true)
            {
                return dialog.FileName;
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
